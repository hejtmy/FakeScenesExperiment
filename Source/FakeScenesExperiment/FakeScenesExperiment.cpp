// Copyright Epic Games, Inc. All Rights Reserved.

#include "FakeScenesExperiment.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FakeScenesExperiment, "FakeScenesExperiment" );
