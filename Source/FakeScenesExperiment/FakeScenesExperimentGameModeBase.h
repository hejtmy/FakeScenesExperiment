// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FakeScenesExperimentGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FAKESCENESEXPERIMENT_API AFakeScenesExperimentGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
